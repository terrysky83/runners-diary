﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;  //needed to reference DbContext and DbSet in Entity Framework
using System.ComponentModel.DataAnnotations;  //provides built-in set of validation attributes

namespace RunnerDiary.Models
{
    public class DailyEntry
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }  //date of the entry

        public string Target { get; set; }  //target distance and run time

        public string My_Run { get; set; }  //actual distance and time

        public string Weather { get; set; }  //Weather of the day

        public string Note { get; set; }  //anything interesting
    }

    public class DiaryDBContext : DbContext
    {
        public DbSet<DailyEntry> RunDiary { get; set; }
    }
}