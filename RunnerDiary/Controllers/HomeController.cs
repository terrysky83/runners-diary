﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RunnerDiary.Models;

namespace RunnerDiary.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Keep Track of Your Running Workout";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HttpGet]
        public ActionResult ContactUs()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ContactUs");
            }

            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(ContactUsInput input)
        {
            if (!ModelState.IsValid)
            {
                if (Request.IsAjaxRequest())
                    return PartialView("_ContactUs", input);

                return View(input);
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("_ThanksForFeedback", input);
            }

            TempData["Message"] = string.Format("Thanks for the feedback, {0}! We will contact you shortly.", input.Name);
            return RedirectToAction("Index");
        }
    }
}
