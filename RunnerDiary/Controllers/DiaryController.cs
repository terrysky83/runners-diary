﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RunnerDiary.Models;

namespace RunnerDiary.Controllers
{
    public class DiaryController : Controller
    {
        private DiaryDBContext db = new DiaryDBContext();

        //
        // GET: /Diary/

        public ActionResult Index()
        {
            return View(db.RunDiary.ToList());
        }

        //
        // GET: /Diary/Details/5

        public ActionResult Details(int id = 0)
        {
            DailyEntry dailyentry = db.RunDiary.Find(id);
            if (dailyentry == null)
            {
                return HttpNotFound();
            }
            return View(dailyentry);
        }

        //
        // GET: /Diary/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Diary/Create

        [HttpPost]
        public ActionResult Create(DailyEntry dailyentry)
        {
            if (ModelState.IsValid)
            {
                db.RunDiary.Add(dailyentry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dailyentry);
        }

        //
        // GET: /Diary/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DailyEntry dailyentry = db.RunDiary.Find(id);
            if (dailyentry == null)
            {
                return HttpNotFound();
            }
            return View(dailyentry);
        }

        //
        // POST: /Diary/Edit/5

        [HttpPost]
        public ActionResult Edit(DailyEntry dailyentry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dailyentry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dailyentry);
        }

        //
        // GET: /Diary/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DailyEntry dailyentry = db.RunDiary.Find(id);
            if (dailyentry == null)
            {
                return HttpNotFound();
            }
            return View(dailyentry);
        }

        //
        // POST: /Diary/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DailyEntry dailyentry = db.RunDiary.Find(id);
            db.RunDiary.Remove(dailyentry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult SearchFunc(string searchString)
        {
            var result = from d in db.RunDiary
                         select d;

            if (!String.IsNullOrEmpty(searchString))
            {
                result = result.Where(s => s.My_Run.Contains(searchString));
            }

            return View(result);
        }
    }
}